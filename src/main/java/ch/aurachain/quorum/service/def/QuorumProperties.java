package ch.aurachain.quorum.service.def;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuorumProperties {

    private String name;
    private String url;
    private List<WalletProperties> wallets;
    private List<TesseraProperties> tesseras;

    public WalletProperties getWallet(String name){
        for (WalletProperties wallet : wallets) {
            if(name.equals(wallet.getName())) {
                return wallet;
            }
        }
        return null;
    }

    public TesseraProperties getTessera(String name){
        for (TesseraProperties tessera : tesseras) {
            if(name.equals(tessera.getName())) {
                return tessera;
            }
        }
        return null;
    }

}
