package ch.aurachain.quorum.service.def;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuorumConfig {

    private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    private String baseDir;
    private String solcDir = "/solc";
    private String contractDir = "/../quorum/contracts";
    private String compileDir = "/compiled";
    private String javaDir = "/java";
    private String walletDir = "/wallet";
    private String javaPackage = "contract";

    private List<QuorumProperties> quorums;

    public String getSolcDir() {
        return baseDir + solcDir;
    }

    public String getContractDir() {
        return baseDir + contractDir;
    }

    public String getCompileDir() {
        return getContractDir() + compileDir;
    }

    public String getJavaDir() {
        return getContractDir() + javaDir;
    }

    public String getWalletDir() {
        return baseDir + walletDir;
    }

    public QuorumProperties getQuorum(String name){
        for (QuorumProperties prop : quorums) {
            if(name.equals(prop.getName())) {
                return prop;
            }
        }
        return null;
    }

    public WalletProperties getWallet(String quorumName, String walletName) {
        QuorumProperties quorum = getQuorum(quorumName);
        return getWallet(quorum, walletName);
    }

    public WalletProperties getWallet(QuorumProperties quorum, String walletName) {
        if(quorum != null) {
            return quorum.getWallet(walletName);
        }
        return null;
    }

    public TesseraProperties getTessera(String quorumName, String tesseraName) {
        QuorumProperties quorum = getQuorum(quorumName);
        return getTessera(quorum, tesseraName);
    }

    public TesseraProperties getTessera(QuorumProperties quorum, String tesseraName) {
        if(quorum != null) {
            return quorum.getTessera(tesseraName);
        }
        return null;
    }

    public static QuorumConfig loadFromYaml(String yamlFilePath) throws IOException {
        return loadFromYaml(new File(yamlFilePath));
    }

    public static QuorumConfig loadFromYaml(File yamlFile) throws IOException {
        return mapper.readValue(yamlFile, QuorumConfig.class);
    }

    public static QuorumConfig loadFromYaml(URL yamlURL) throws IOException {
        return mapper.readValue(yamlURL, QuorumConfig.class);
    }

    public static QuorumConfig loadDefault() throws IOException {
        return loadFromYaml(QuorumConfig.class.getResource("/quorum/config/quorum.yml"));
    }
}
