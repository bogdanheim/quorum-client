package ch.aurachain.quorum;

import ch.aurachain.quorum.service.def.QuorumConfig;
import ch.aurachain.quorum.service.def.QuorumProperties;
import ch.aurachain.quorum.service.def.TesseraProperties;
import ch.aurachain.quorum.service.def.WalletProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import okhttp3.OkHttpClient;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.web3j.codegen.SolidityFunctionWrapperGenerator;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.quorum.Quorum;
import org.web3j.quorum.enclave.Enclave;
import org.web3j.quorum.enclave.Tessera;
import org.web3j.quorum.enclave.protocol.EnclaveService;
import org.web3j.quorum.tx.QuorumTransactionManager;
import org.web3j.tx.TransactionManager;

import javax.tools.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

@SpringBootApplication
public class QuorumApplication {


    public static void main(String[] args) {

        //SpringApplication.run(QuorumApplication.class, args);
        try {

            String smartContractName = "Test";

            QuorumConfig quorumConfig = QuorumConfig.loadDefault();
            QuorumProperties quorumProperties = quorumConfig.getQuorum("quorum1");
            Quorum quorum = Quorum.build(new HttpService(quorumProperties.getUrl()));

//            Credentials credentials = WalletUtils
//                    .loadCredentials(BC_ETH_PASS, BASE_DIR + "/wallet/" + BLOCKCHAIN_ETH_WALLET);
            WalletProperties walletProperties = quorumProperties.getWallet("key1");
            Credentials credentials = WalletUtils
                    .loadCredentials(walletProperties.getPassword(), quorumConfig.getWalletDir() + "/" + walletProperties.getFileName());
            String methodName = "prima";
            Object[] actualParams = new Object[]{"testString"};


            //PRIVATE
            TesseraProperties tesseraProperties = quorumProperties.getTessera("TESSERA1");
            log("initialize the enclave service using the tessera ThirdParty app URL");
            EnclaveService enclaveService = new EnclaveService(tesseraProperties.getHost(), tesseraProperties.getPort(), new OkHttpClient());
            log("initialize the tessera enclave");
            Enclave enclave = new Tessera(enclaveService, quorum);
            log("saveSmartContract: Load credentials...");
            QuorumTransactionManager qrtxm = new QuorumTransactionManager(quorum,
                    credentials,
                    tesseraProperties.getPublicKey(),
                    Arrays.asList(quorumProperties.getTessera("TESSERA7").getPublicKey()),
                    enclave);
            Object[] constructorParams = new Object[]{quorum, qrtxm, BigInteger.valueOf(0), BigInteger.valueOf(4_300_000)};
            String smartContractAddress = deploy(quorumConfig, smartContractName, constructorParams, true);
            Class<?> aClass = getaClass(quorumConfig,smartContractName);
            executeMethod(quorum, qrtxm, aClass, smartContractAddress, methodName, actualParams);
//            //PUBLIC
//
//            Object[] constructorParams = new Object[]{quorum, credentials, BigInteger.valueOf(0), BigInteger.valueOf(4_300_000)};
//            String smartContractAddress = deploy(quorumConfig, smartContractName, constructorParams, false);
//            Class<?> aClass = getaClass(quorumConfig,smartContractName);
//            executeMethod(quorum, credentials, aClass, smartContractAddress, methodName, actualParams);
//            //=============================
//            // DEPLOY
//            //=============================
//
//
//            //=============================
//            // EXECUTE
//            //=============================
//            //String smartContractAddress = "0x1932c48b2bf8102ba33b4a6b545c32236e342f34";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void executeMethod(Quorum quorum, QuorumTransactionManager qrtxm, Class<?> aClass, String smartContractAddress, String methodName, Object[] actualParams) throws Exception {
        Method loadMethod = getLoadMethod(aClass, true);
        Class[] paramClasses = getParamClasses(actualParams);

        Method smartContractMethod = aClass.getMethod(methodName, paramClasses);
        Object instance = invokeLoadMethod(smartContractAddress, quorum, qrtxm, loadMethod);

        executeMethod2(methodName, actualParams, aClass, smartContractMethod, instance);
    }

    private static void executeMethod(Quorum quorum, Credentials credentials, Class<?> aClass, String smartContractAddress, String methodName, Object[] actualParams) throws Exception {
        Method loadMethod = getLoadMethod(aClass, false);
        Class[] paramClasses = getParamClasses(actualParams);

        Method smartContractMethod = aClass.getMethod(methodName, paramClasses);
        Object instance = invokeLoadMethod(smartContractAddress, quorum, credentials, loadMethod);

        executeMethod2(methodName, actualParams, aClass, smartContractMethod, instance);
    }

    private static void executeMethod2(String methodName, Object[] actualParams, Class<?> aClass, Method smartContractMethod, Object instance) throws Exception {
        Object remoteCall = smartContractMethod.invoke(instance, actualParams);
        log("executeEthereumContractMethod: Call method...");
        TransactionReceipt tr = (TransactionReceipt) ((RemoteCall) remoteCall).send();

        if (StringUtils.equals("0x1", tr.getStatus())) {
            List<Object> re = getEvents(methodName, aClass, instance, tr);
            for (Object obj : re) {
                Map<String, Object> data = new HashMap<>();
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field field : fields) {
                    try {
                        if (!field.getName().equals("log")) {
                            data.put(field.getName(), field.get(obj));
                        }
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        log("Error reading event data");
                        throw e;
                    }
                }

                String responsePayload = new ObjectMapper().writeValueAsString(data);
                log("executeEthereumContractMethod: Response payload: " + responsePayload);
            }
        }
    }

    private static List<Object> getEvents(String methodName, Class<?> aClass, Object instance, TransactionReceipt tr) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method smartContractMethod4 = aClass
                .getMethod("get" + StringUtils.capitalize(methodName) + "EventEvents",
                        new Class[]{TransactionReceipt.class});
        Object remoteCall4 = smartContractMethod4.invoke(instance, new Object[]{tr});
        return (List<Object>) remoteCall4;
    }

    private static Class[] getParamClasses(Object[] actualParams) {
        Class[] paramClasses = new Class[actualParams.length];
        for (int i = 0; i < actualParams.length; i++) {
            paramClasses[i] = actualParams[i].getClass();
        }
        return paramClasses;
    }

    private static String deploy(QuorumConfig quorumConfig, String smartContractName, Object[] constructorParams, Boolean isPrivate) throws Exception {

        File compiledDirFile = getDirectoryEmpty(quorumConfig.getCompileDir());
        File smartContractFile = new File(quorumConfig.getContractDir(), smartContractName + ".sol");

        compileSolidity(quorumConfig, smartContractFile);

        getDirectoryEmpty(quorumConfig.getJavaDir());

        createSolidotyJavaWrapper(quorumConfig, smartContractName);
        ethereumJavaCompile(quorumConfig, smartContractName);

        Class<?> aClass = getaClass(quorumConfig, smartContractName);

        int i = 0;
        Class[] paramClasses = new Class[4];
        paramClasses[i++] = Web3j.class;
        paramClasses[i++] = isPrivate ? TransactionManager.class : Credentials.class;
        paramClasses[i++] = BigInteger.class;
        paramClasses[i++] = BigInteger.class;

        Method smartContractConstructor = aClass.getMethod("deploy", paramClasses);

        log("saveSmartContract: Deploy SmartContract...");
        RemoteCall rc = (RemoteCall) smartContractConstructor.invoke(null, constructorParams);
        Object instance = rc.send();

        Method getContractAddressMethod = aClass.getMethod("getContractAddress");

        log("saveSmartContract: Get SmartContractAddress...");
        String address = (String) getContractAddressMethod.invoke(instance);
        log(address);
        return address;
    }

    private static Object invokeLoadMethod(String ethereumAddress, Web3j web3j, TransactionManager qtxmgr, Method loadMethod)
            throws IllegalAccessException, InvocationTargetException, IOException, CipherException {
        return loadMethod
                .invoke(null, ethereumAddress, web3j, qtxmgr, BigInteger.valueOf(0), BigInteger.valueOf(4_300_000));
    }

    private static Object invokeLoadMethod(String ethereumAddress, Web3j web3j, Credentials credentials, Method loadMethod)
            throws IllegalAccessException, InvocationTargetException, IOException, CipherException {
        return loadMethod
                .invoke(null, ethereumAddress, web3j, credentials, BigInteger.valueOf(0), BigInteger.valueOf(4_300_000));
    }

    private static Method getLoadMethod(Class<?> aClass, Boolean isPrivate) throws NoSuchMethodException {
        return aClass.getMethod("load",
                new Class[]{String.class, Web3j.class, isPrivate ? TransactionManager.class : Credentials.class, BigInteger.class,
                        BigInteger.class});
    }

    private static Class<?> getaClass(QuorumConfig quorumConfig, String smartContractName) throws MalformedURLException, ClassNotFoundException {
        URLClassLoader classLoader = new URLClassLoader(
                new URL[]{(new File(quorumConfig.getJavaDir()))
                        .toURI().toURL()},
                QuorumApplication.class.getClassLoader());

        return Class.forName(
                quorumConfig.getJavaPackage() + "." + smartContractName, true,
                classLoader);
    }

    private static void createSolidotyJavaWrapper(QuorumConfig quorumConfig, String smartContractName) {
        String[] arguments = new String[]{"-b=" + quorumConfig.getCompileDir() + "/" + smartContractName + ".bin",
                "-a=" + quorumConfig.getCompileDir() + "/" + smartContractName + ".abi", "-p", quorumConfig.getJavaPackage(),
                "-o",
                quorumConfig.getJavaDir()};
        SolidityFunctionWrapperGenerator.main(arguments);
    }

    private static File getDirectoryEmpty(String compiledDir) throws IOException {
        File dirFile = new File(compiledDir);
        dirFile.mkdirs();
        FileUtils.cleanDirectory(dirFile);
        return dirFile;
    }

    private static void compileSolidity(QuorumConfig quorumConfig, File smartContractFile) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime()
                .exec(quorumConfig.getSolcDir() + "/solc.exe -o " + quorumConfig.getCompileDir() + " --optimize --abi --bin " + smartContractFile.getAbsolutePath());
        BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        process.waitFor();
        String result = IOUtils.toString(stdError);
        File compiledDirFile = new File(quorumConfig.getCompileDir());
        if (compiledDirFile.list().length == 0) {
            throw new RuntimeException("Error during solc:" + result);
        }
    }

    private static void ethereumJavaCompile(QuorumConfig quorumConfig, String smartContractName) throws IOException {

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnosticCollector = new DiagnosticCollector<>();
        StandardJavaFileManager fileManager =
                compiler.getStandardFileManager(diagnosticCollector, Locale.getDefault(), null);

        File sourceFile = new File(quorumConfig.getJavaDir(), quorumConfig.getJavaPackage() + "/" + smartContractName + ".java");
        Iterable<? extends JavaFileObject> compilationUnits =
                fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile));

        List<String> options = new ArrayList<>();
        options.add("-classpath");
        StringBuilder sb = new StringBuilder();
        Enumeration<URL> resources = QuorumApplication.class.getClassLoader().getResources("/");
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            sb.append(url.getFile()).append(File.pathSeparator);
        }

        File libFolder = new File(quorumConfig.getBaseDir(), "lib");
        if (libFolder.exists()) {
            for (File file : libFolder.listFiles()) {
                if (file.isFile()) {
                    sb.append(file.getAbsolutePath()).append(File.pathSeparator);
                }
            }
        }

        options.add(sb.toString());

        Boolean result =
                compiler.getTask(null, fileManager, diagnosticCollector, options, null, compilationUnits).call();
        if (!result) {
            StringBuilder errorStringBuilder = new StringBuilder();
            List<Diagnostic<? extends JavaFileObject>> diagnostics = diagnosticCollector.getDiagnostics();
            for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics) {
                errorStringBuilder.append(diagnostic.getMessage(null)).append("\n");
            }
            throw new RuntimeException("Error during java compile:" + errorStringBuilder.toString());
        }
    }

    public static void log(String message) {
        System.out.println(message);
    }
}
