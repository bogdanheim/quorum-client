pragma solidity >=0.4.22 <0.6.0;

contract Test {

    struct clasa {
        string str1;
        string str2;
    }

    constructor() public {

    }

    event primaEvent( string var2 );

    function prima( string memory var1 ) public returns ( string memory var2 )    {
        var2 = var1;

        emit primaEvent( var2 );

        return var2;
    }
}
